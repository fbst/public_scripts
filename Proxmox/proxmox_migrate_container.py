import urllib3
import argparse
import requests
import time

urllib3.disable_warnings()


###### Definitiions #######
#TemplateName = "CT-ubuntu22.04"
PROXMOX_HOST = 'https://proxmox.homelab.local:8006'
API_TOKEN = 'PVEAPIToken=api@pam!api_scripts_token_id=c736dcd7-077a-4660-990b-5f990b16249f'
headers = {
    'Authorization': f'{API_TOKEN}'
}



# get all vmids and write them into list
# get all ids of vm and container and put them in list
# take the higher id and add 1 to get the new ID of the CT to be created
def migrateCT(containerId,targetNode):
    try:
        response = requests.post(f"{PROXMOX_HOST}/api2/json/nodes/{NodeName}/lxc/{ctId}/status/start", headers=headers, verify=False)
        if response.status_code == 200:
            print(f"Container '{CTName}' has been started successfully")
        else:
            print(f"Error while trying to start container: {response.text}")
    except requests.exceptions.RequestException as e:
        print(f"Request error: {str(e)}")
    except Exception as e:
        print(f"Error: {str(e)}")

    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Migrer un conteneur d'un noeur proxmox à un autre")
    parser.add_argument('containerId', help="ID du conteneur à migrer")
    parser.add_argument('destinationNode', help="Noeud vers lequel déplacer le conteneur")
    args = parser.parse_args()

    migrateCT(args.containerID,args.destinationNode)
    



