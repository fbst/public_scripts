##############################################################################################
#####                                                                                    #####
##### Usage: python <ProxmoxNodeName> <NewContainerName> <ProxmoxTemplateName>           #####
#####                                                                                    #####
##### All arguments are mandatory, if they are not present, the script will fail         #####
##############################################################################################

import urllib3
import argparse
import requests
import time
import os

## Used to avoid too many warnings in case of self signed certificats
## if you have public legit certificate you can comment this and change the verifySSLCerts variable to True
urllib3.disable_warnings()

###### Definitions #######
verifySSLCerts=False

### Create environment variables to store Proxmox URL and API Tokens 

PROXMOX_HOST_URL = os.getenv('PROXMOX_HOST_URL')
PROXMOX_API_TOKEN = os.getenv('PROXMOX_API_TOKEN')

headers = {
    'Authorization': f'{PROXMOX_API_TOKEN}'
}

# get all ids of vm and container and put them in list
# take the higher id and add 1 to get the new ID of the CT to be created
def getNewCTId(NodeName):
    ids = []
    response_vm = requests.get(f"{PROXMOX_HOST_URL}/api2/json/nodes/proxmox/qemu",headers=headers, verify=verifySSLCerts)
    response_lxc = requests.get(f"{PROXMOX_HOST_URL}/api2/json/nodes/proxmox/lxc",headers=headers, verify=verifySSLCerts)
    for vm in response_vm.json()['data']:
        ids.append(vm['vmid'])
    for lxc in response_lxc.json()['data']:
        ids.append(int(lxc['vmid']))
    higherId = max(ids)  
    return higherId+1


### use the name of the template provided in the script call arguments and return the id of the template
def getTemplateId(TemplateName,NodeName):
    
    response = requests.get(f"{PROXMOX_HOST_URL}/api2/json/nodes/proxmox/lxc",headers=headers, verify=verifySSLCerts)
    vmid_count = 0
    vmids=[]
    for item in response.json()['data']:
        if 'vmid' in item:
            vmid_count += 1
    for i in range(0,vmid_count):
        if 'template' in response.json()['data'][i]:
            return response.json()['data'][i]['vmid']

### start newly created container on a specific node

def startNewlyCreatedContainer(NodeName,ctId,CTName):
    try:
        response = requests.post(f"{PROXMOX_HOST_URL}/api2/json/nodes/{NodeName}/lxc/{ctId}/status/start", headers=headers, verify=verifySSLCerts)
        if response.status_code == 200:
            print(f"Container '{CTName}' has been started successfully")
        else:
            print(f"Error while trying to start container: {response.text}")
    except requests.exceptions.RequestException as e:
        print(f"Request error: {str(e)}")
    except Exception as e:
        print(f"Error: {str(e)}")

### create Container on a specific node, from a template

def createCTFromTemplate(TemplateName,NodeName,NewCTName):    
    try:
        newCTId = getNewCTId(NodeName)
        data = {
                'newid': newCTId,
                'hostname': NewCTName,
                'full': 1
        }
        response = requests.post(f"{PROXMOX_HOST_URL}/api2/json/nodes/{NodeName}/lxc/{getTemplateId(TemplateName,NodeName)}/clone", json=data, headers=headers, verify=verifySSLCerts)
        if response.status_code == 200:
            print(f"Container '{NewCTName}' (VMID: {newCTId}) created successfully by cloning the template {TemplateName} and will be now started...")
            time.sleep(10)
            startNewlyCreatedContainer(NodeName,newCTId,NewCTName)
        else:
            print(f"Error creating container: {response.text}")
    except requests.exceptions.RequestException as e:
        print(f"Request error: {str(e)}")
    except Exception as e:
        print(f"Error: {str(e)}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Créer un conteneur LXC dans proxmox à partir d'un template.")
    parser.add_argument('nodeName', help="Nom du noeud proxmox")
    parser.add_argument('newContainerName', help="Nom du nouveau conteneur à créer")
    parser.add_argument('templateName', help="Nom du Template à partir duquel le conteneur sera créé")
  
    args = parser.parse_args()
    
    ### Function CALLS ###
    createCTFromTemplate(args.templateName,args.nodeName,args.newContainerName)
    



