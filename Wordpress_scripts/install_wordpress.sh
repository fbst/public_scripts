#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

# Download wordpress cli
sudo curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
sudo chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

# accept user input
echo "Database Name: "
read -e dbname

echo "Database User: "
read -e dbuser

echo "Database Password: "
read -e -s dbpassword

echo "WP username: "
read -e wpuser

echo "WP email: "
read -e EMAIL

echo "WP Password: "
read -e -s wppassword

echo "WP URL: "
read -e SITEURL

# accept the name of our website
echo "Site Title: "
read -e wptitle

# accept the name of our website
echo "Language (e.g: fr_FR ): "
read -e wplang

sudo sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf
sudo apt-get -qq update 
sudo apt-get -qq -y upgrade
sudo apt-get -qq -y install mariadb-server

[ ! -e /usr/bin/expect ] && { sudo apt-get -y install expect; }
SECURE_MYSQL=$(sudo expect -c "

set timeout 10
spawn mysql_secure_installation

expect \"Enter current password for root (enter for none): \"
send \"n\r\"
expect \"Switch to unix_socket authentication \[Y/n\] \"
send \"n\r\"
expect \"Change the root password? \[Y/n\] \"
send \"n\r\"
expect \"Remove anonymous users? \[Y/n\] \"
send \"y\r\"
expect \"Disallow root login remotely? \[Y/n\] \"
send \"y\r\"
expect \"Remove test database and access to it? \[Y/n\] \"
send \"y\r\"
expect \"Reload privilege tables now? \[Y/n\] \"
send \"y\r\"
expect eof
")

sudo mysql -e "CREATE DATABASE IF NOT EXISTS $dbname CHARACTER SET utf8 COLLATE utf8_unicode_ci; CREATE USER '$dbuser'@'localhost' IDENTIFIED BY '$dbpassword'; GRANT ALL PRIVILEGES ON $dbname.* TO '$dbuser'@'localhost';FLUSH PRIVILEGES;"
sudo apt -qq -y install php-soap php-fpm php-curl php-intl php-mbstring php-gd php-zip php-xml php-mysql php-imap php-imagick php-redis nginx-extras
sudo mkdir -p /var/www/users/wor /var/www/users/wor/home /var/www/users/wor/${SITEURL} /var/www/users/wor/${SITEURL}/app /var/www/users/wor/${SITEURL}/data /var/www/users/wor/${SITEURL}/scripts /var/www/users/wor/${SITEURL}/log /var/www/users/wor/${SITEURL}/tmp
sudo useradd wor --home /var/www/users/wor/home --gid users --shell /bin/bash
sudo chmod 0751 /var/www/users /var/www/users/wor /var/www/users/wor/home /var/www/users/wor/${SITEURL} /var/www/users/wor/${SITEURL}/app
sudo chmod 0701 /var/www/users/wor/${SITEURL}/tmp
sudo chmod 0710 /var/www/users/wor/${SITEURL}/data
sudo chmod 0700 /var/www/users/wor/${SITEURL}/scripts /var/www/users/wor/${SITEURL}/log
sudo chgrp users /var/www/users/wor /var/www/users/wor/${SITEURL}
sudo chown wor:users /var/www/users/wor/home /var/www/users/wor/${SITEURL}/app /var/www/users/wor/${SITEURL}/log /var/www/users/wor/${SITEURL}/tmp /var/www/users/wor/${SITEURL}/scripts
sudo chown wor:www-data /var/www/users/wor/${SITEURL}/data
echo $dbpassword | passwd wor --quiet
runuser -l wor -c "wget https://fr.wordpress.org/wordpress-latest-fr_FR.tar.gz"
runuser -l wor -c "tar xvzf wordpress-latest-fr_FR.tar.gz -C /var/www/users/wor/${SITEURL}/app"
sudo ln -s /var/www/users/wor/${SITEURL}/app/wordpress /var/www/users/wor/${SITEURL}/web
sudo touch /etc/php/8.1/fpm/pool.d/wordpress.conf
echo "creating wp-config.php ...."
wp core config --dbname=$dbname --dbuser=$dbuser --dbpass=$dbpass --extra-php <<PHP
define('AUTOSAVE_INTERVAL', 300 );
define('WP_POST_REVISIONS', false );
define( 'WP_AUTO_UPDATE_CORE', true );
define( 'WP_DEBUG', false );
PHP

echo "[wordpress]
user = wor
group = users
listen = /var/run/php7-fpm-wor.sock
listen.owner = www-data
listen.group = www-data
listen.mode = 0666
pm = dynamic
pm.max_children = 10
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 5
pm.max_requests = 1000
catch_workers_output = yes
security.limit_extensions = .php
php_value[include_path] = .
php_admin_value[error_log] = /var/www/users/wor/SITEURL/log/php-fpm.error.log
php_admin_value[max_input_time] = 180
php_admin_value[max_input_vars] = 3000
php_admin_value[memory_limit] = 256M
php_admin_value[open_basedir] = /var/www/users/wor/SITEURL/web/:/var/www/users/wor/SITEURL/app/:/var/www/users/wor/SITEURL/data/:/var/www/users/wor/SITEURL/log/:/var/www/users/wor/SITEURL/tmp/
php_admin_value[upload_tmp_dir] = /var/www/users/wor/SITEURL/tmp/
" >> /etc/php/8.1/fpm/pool.d/wordpress.conf

sudo sed -i "s/SITEURL/${SITEURL}/g" /etc/php/8.1/fpm/pool.d/wordpress.conf
sudo systemctl restart php8.1-fpm
sudo apt -qq -y install certbot
sudo certbot certonly --standalone --agree-tos --no-eff-email -m ${EMAIL} -d ${SITEURL} --pre-hook "systemctl stop nginx" --post-hook "systemctl start nginx"
sudo rm /etc/nginx/sites-enabled/default
sudo mkdir /etc/nginx/includes
sudo mkdir -p /var/acme/.well-known/acme-challenge/
sudo touch /etc/nginx/includes/acme.conf
sudo echo "
location ^~ /.well-known/acme-challenge/ {
    root      /var/acme;

    autoindex off;
    index     index.html;
    try_files $uri $uri/ =404;
    auth_basic off;
}
">>/etc/nginx/includes/acme.conf
sudo touch /etc/nginx/sites-available/${SITEURL}.conf
sudo echo "
server {
  listen *:80;
  server_name      SITEURL;

  index            index.html index.php;
  access_log       /var/log/nginx/SITEURL.access.log combined;
  error_log        /var/log/nginx/SITEURL.error.log;

  location / {
    index     index.html index.php;
    rewrite ^ https://$server_name$request_uri? permanent;
  }
  include includes/acme.conf;
}
">>/etc/nginx/sites-available/${SITEURL}.conf
sudo touch /etc/nginx/sites-available/${SITEURL}_ssl.conf
sudo echo "
server {
  listen       *:443 ssl http2;
  listen       [::]:443 ssl http2 ;
  server_name  SITEURL;

  ssl_certificate           /etc/letsencrypt/live/SITEURL/fullchain.pem;
  ssl_certificate_key       /etc/letsencrypt/live/SITEURL/privkey.pem;
  ssl_session_cache         shared:SSL:20m;
  ssl_session_timeout       1d;
  ssl_session_tickets       off;
  ssl_buffer_size           4k;
  ssl_protocols             TLSv1.2 TLSv1.3;
  ssl_ciphers               ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA;
  ssl_prefer_server_ciphers off;
  ssl_stapling              on;
  ssl_stapling_verify       on;

  index  index.html index.php;
  access_log            /var/log/nginx/ssl-SITEURL_ssl.access.log combined;
  error_log             /var/log/nginx/ssl-SITEURL_ssl.error.log;
  root /var/www/users/wor/SITEURL/web/;


  location ~ (.+\.php)(.*)$ {
    include       /etc/nginx/fastcgi.conf;

    fastcgi_pass  unix:/var/run/php7-fpm-wor.sock;
    fastcgi_index index.php;
    fastcgi_split_path_info ^(.+\.php)(.*)$;
    fastcgi_param PATH_INFO \$fastcgi_path_info;
    fastcgi_param SCRIPT_FILENAME \$document_root$fastcgi_script_name;
    fastcgi_hide_header X-Frame-Options;
    fastcgi_intercept_errors on;
  }
}
">>/etc/nginx/sites-available/${SITEURL}_ssl.conf

sudo sed -i "s/SITEURL/${SITEURL}/g" /etc/nginx/sites-available/*
cd /etc/nginx/sites-enabled
sudo ln -s ../sites-available/${SITEURL}.conf
sudo ln -s ../sites-available/${SITEURL}_ssl.conf
sudo systemctl restart nginx