#!/bin/bash
apt install -y zip nfs-common msmtp msmtp-mta
# Backup source directories
BACKUP_DEST=""
RETENTION_DAYS=5

HOSTNAME=`hostname`
TODAY=$(date +%Y-%m-%d)
DESTINATION="$BACKUP_DEST/$HOSTNAME/$TODAY"

NFS_SERVER=""
NFS_VERSION="4.1"
NFS_MOUNT=""

# MySQL settings
MYSQL_USER=""
MYSQL_PASS=""
MYSQL_HOST="localhost"
# Databases to exclude (space-separated list)
EXCLUDE_DB=("information_schema" "performance_schema" "mysql" "sys" "phpmyadmin")

# Email settings
SMTP_SERVER=""
SMTP_PORT="587"
SMTP_USER=""
SMTP_PASS=""
SMTP_FROM=""
RECIPIENT_EMAIL=""

mkdir -p "$BACKUP_DEST/$HOSTNAME/$TODAY"
# Function to create the ~/.msmtprc file
create_msmtprc() {
    echo "Creating ~/.msmtprc file..."

    cat > ~/.msmtprc <<EOF
defaults
tls on
tls_starttls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt

account default
host $SMTP_SERVER
port $SMTP_PORT
auth on
user $SMTP_USER
password $SMTP_PASS
from $SMTP_FROM
EOF

    chmod 600 ~/.msmtprc
}

# Check for ~/.msmtprc file and create if not exists
if [ ! -f ~/.msmtprc ]; then
    create_msmtprc
fi

# Function to send an email
send_email() {
    EMAIL_SUBJECT="$1"
    EMAIL_BODY="$2"

    echo -e "Subject: $EMAIL_SUBJECT\n\n$EMAIL_BODY" | msmtp $RECIPIENT_EMAIL
}


# Check if NFS mount is available
if mountpoint -q "$BACKUP_DEST"; then
    echo "NFS is already mounted."
else
    echo "Mounting NFS..."
    if ! mount.nfs -o vers="$NFS_VERSION" "$NFS_SERVER:$NFS_MOUNT" "$BACKUP_DEST"; then
       send_email "Backup Failure Alert" "Failed to mount NFS at $NFS_MOUNT on $NFS_SERVER."
       exit 1
    fi
fi

# Function to create database backups
backup_databases() {
    # Get the list of databases
    databases=$(mysql -h $MYSQL_HOST -u $MYSQL_USER -p$MYSQL_PASS -e "SHOW DATABASES;" | grep -v -w Database)
    # Loop through databases and create a backup for each
    for db in $databases; do
        if [[ ! " ${EXCLUDE_DB[@]} " =~ " ${db} " ]]; then
            echo "Backing up database: $db"
            mysqldump -h $MYSQL_HOST -u $MYSQL_USER -p$MYSQL_PASS --databases $db | gzip > "$DESTINATION/$db-$TODAY.sql.gz"
        else
            echo "Skipping database: $db"
        fi
    done
}


# Create backups for databases
backup_databases

# Cleanup old backups
if ! find "$BACKUP_DEST/$HOSTNAME" -mindepth 1 -maxdepth 1 -type d | sort -r | tail -n +"$((RETENTION_DAYS+1))" | xargs -r rm -rf; then
    send_email "Backup Cleanup Failure Alert" "Failed to clean up old backups in $BACKUP_DEST."
    exit 1
fi

echo "Backup and cleanup process completed."


