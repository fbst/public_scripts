#!/bin/bash
apt install -y zip nfs-common msmtp msmtp-mta
# Define backup variables
SYNERGAIA_SOURCE_FOLDER=("/var/lib/synergaia" "/var/lib/couchdb")
BACKUP_DEST=""
RETENTION_DAYS=5

# NFS Mountpoint details
NFS_SERVER=""
NFS_VERSION="4.1"
NFS_MOUNT=""

HOSTNAME=`hostname`

# Email settings
SMTP_SERVER=""
SMTP_PORT="587"
SMTP_USER=""
SMTP_PASS=""
SMTP_FROM=""
RECIPIENT_EMAIL=""


# Function to create the ~/.msmtprc file
create_msmtprc() {
    echo "Creating ~/.msmtprc file..."

    cat > ~/.msmtprc <<EOF
defaults
tls on
tls_starttls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt

account default
host $SMTP_SERVER
port $SMTP_PORT
auth on
user $SMTP_USER
password $SMTP_PASS
from $SMTP_FROM
EOF

    chmod 600 ~/.msmtprc
}

# Check for ~/.msmtprc file and create if not exists
if [ ! -f ~/.msmtprc ]; then
    create_msmtprc
fi

# Function to send an email
send_email() {
    EMAIL_SUBJECT="$1"
    EMAIL_BODY="$2"

    echo -e "Subject: $EMAIL_SUBJECT\n\n$EMAIL_BODY" | msmtp $RECIPIENT_EMAIL
}


# Check if NFS mount is available
if mountpoint -q "$BACKUP_DEST"; then
    echo "NFS is already mounted."
else
    echo "Mounting NFS..."
    if ! mount.nfs -o vers="$NFS_VERSION" "$NFS_SERVER:$NFS_MOUNT" "$BACKUP_DEST"; then
       send_email "Backup Failure Alert on $HOSTNAME" "Failed to mount NFS at $NFS_MOUNT on $NFS_SERVER."
       exit 1
    fi
fi

# Proceed with backup
TODAY=$(date +%Y-%m-%d)
DESTINATION="$BACKUP_DEST/$HOSTNAME/$TODAY"
mkdir -p "$DESTINATION"
BACKUP_NAME="backup-$(date +%Y-%m-%d-%H-%M-%S).zip"

# Create a compressed backup of multiple folders
ZIP_CMD="zip -r \"$DESTINATION/$BACKUP_NAME\""
for folder in "${SYNERGAIA_SOURCE_FOLDER[@]}"; do
    ZIP_CMD+=" \"$folder\""
done

# Execute the ZIP command and check for errors
if ! eval $ZIP_CMD; then
    send_email "Backup Failure Alert on $HOSTNAME" "Failed to create backup at $DESTINATION/$BACKUP_NAME on $HOSTNAME."
    exit 1
fi

# Cleanup old backups
if ! find "$BACKUP_DEST/$HOSTNAME" -mindepth 1 -maxdepth 1 -type d | sort -r | tail -n +"$((RETENTION_DAYS+1))" | xargs -r rm -rf; then
    send_email "Backup Cleanup Failure Alert on $HOSTNAME" "Failed to clean up old backups in $BACKUP_DEST on $HOSTNAME."
    exit 1
fi

echo "Backup and cleanup process completed."
