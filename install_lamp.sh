#!/bin/sh


#VARIABLES

APACHE_SECURITY_CONFIG_FILE='/etc/apache2/conf-enabled/security.conf'
APACHE_MODSECURITY_CONFIG_FILE='/etc/modsecurity/modsecurity.conf'
#COLORS
# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan

echo -e "$Cyan \n Updating System.. $Color_Off"
apt update -y && sudo apt upgrade -y

## Install AMP
echo -e "$Cyan \n Installing Apache2 $Color_Off"
apt install apache2 apache2-doc apache2-utils libexpat1 ssl-cert -y

echo -e "$Cyan \n Installing PHP & Requirements $Color_Off"
apt install php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap -y

echo -e "$Cyan \n Installing MySQL $Color_Off"
apt install mysql-server mysql-client libmysqlclient-dev -y

#echo -e "$Cyan \n Installing phpMyAdmin $Color_Off"
#apt-get install phpmyadmin -y

echo -e "$Cyan \n Verifying installs$Color_Off"
apt install apache2 libapache2-mod-php php mysql-server php-pear php-mysql mysql-client mysql-server php-mysql php-gd libapache2-mod-security2 libapache2-mod-evasive -y

## TWEAKS and Settings
# Permissions
echo -e "$Cyan \n Permissions for /var/www $Color_Off"
chown -R www-data:www-data /var/www
echo -e "$Green \n Permissions have been set $Color_Off"

# Enabling Mod Rewrite, required for WordPress permalinks and .htaccess files
echo -e "$Cyan \n Enabling Modules $Color_Off"
a2enmod rewrite
a2enmod security2

#basic hardening of apache2
echo -n '> configuring modsecurity 2...'
mv /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf
sed -i -e 's/SecRuleEngine DetectionOnly/SecRuleEngine On/' $APACHE_MODSECURITY_CONFIG_FILE
sed -i -e 's/SecAuditLogParts ABDEFHIJZ/SecAuditLogParts ABCEFHJKZ/' $APACHE_MODSECURITY_CONFIG_FILE
cd /tmp
wget https://github.com/coreruleset/coreruleset/archive/refs/tags/v3.3.5.tar.gz &>/dev/null
tar xf v3.3.5.tar.gz &>/dev/null
mkdir -p /etc/apache2/modsecurity-crs/
mv '/tmp/coreruleset-3.3.5/' /etc/apache2/modsecurity-crs/
rm /tmp/v3.3.5.tar.gz
mv /etc/apache2/modsecurity-crs/coreruleset-3.3.5/crs-setup.conf.example /etc/apache2/modsecurity-crs/coreruleset-3.3.5/crs-setup.conf

echo 'IncludeOptional /etc/apache2/modsecurity-crs/coreruleset-3.3.5/crs-setup.conf' >> /etc/apache2/mods-enabled/security2.conf
echo 'IncludeOptional /etc/apache2/modsecurity-crs/coreruleset-3.3.5/rules/*.conf' >> /etc/apache2/mods-enabled/security2.conf

touch /etc/logrotate.d/modsec
echo '/var/log/apache2/modsec_audit.log
{
        rotate 31
        daily
        missingok
        compress
        delaycompress
        notifempty
}' > /etc/logrotate.d/modsec

apache2ctl -t
systemctl restart apache2

echo -n '> Enabling Protection Against Fingerprinting... '

sed -i -e 's/ServerSignature On/ServerSignature Off/' $APACHE_SECURITY_CONFIG_FILE
sed -i -e 's/ServerTokens OS/ServerTokens Prod/' $APACHE_SECURITY_CONFIG_FILE

SUCCESS=$?

if [ $SUCCESS -eq 0 ]; then
    echo "Apache2 Fingerprinting protection configuration: [OK]"
else
    echo "Apache2 Fingerprinting protection configuration: [ERROR]"
fi

# Limit Request Size To Prevent DOS

echo -n '> imit Request Size... '

sed -r -i -e 's|^([[:space:]]*)</Directory>|\1\tLimitRequestBody 512000\n\1\tOptions -FollowSymLinks\n\1</Directory>|g' $APACHE_SECURITY_CONFIG_FILE 

SUCCESS=$?

if [ $SUCCESS -eq 0 ]; then
    echo "[OK]"
else
    echo "[ERROR]"
fi


# Disable Risky HTTP Methods

echo -n '> Disable Risky HTTP Methods... '

sed -r -i -e 's|^([[:space:]]*)</Directory>|\n\1\t<LimitExcept GET POST HEAD>\n\1\t\tdeny from all\n\1\t</LimitExcept>\n\n</Directory>|g' $APACHE_SECURITY_CONFIG_FILE

SUCCESS=$?

if [ $SUCCESS -eq 0 ]; then
    echo "Apache disabling risky http methods: $Green[OK]$Color_Off"
else
    echo "Apache disabling risky http methods: $Red[ERROR]$Color_Off"
fi


# Enable XSS Protection For Modern Browsers

echo -n '> Enable XSS Protection For Modern Browsers... '

echo '' >> $APACHE_SECURITY_CONFIG_FILE 
echo '<IfModule mod_headers.c>' >> $APACHE_SECURITY_CONFIG_FILE 
echo 'Header set X-XSS-Protection 0' >> $APACHE_SECURITY_CONFIG_FILE 
echo '</IfModule>' >> $APACHE_SECURITY_CONFIG_FILE 

SUCCESS=$?

if [ $SUCCESS -eq 0 ]; then
    echo "Apache XSS Protection config: $Green[OK]$Color_Off"
else
    echo "Apache XSS Protection config: $Red[ERROR]$Color_Off"
fi


# Protect Apache Binary Files

echo -n '> Protect Apache Config Files... '

chown -R root:root /etc/apache2/apache2.conf >/dev/null 2>&1 &
chmod -R 750 /etc/apache2/apache2.conf >/dev/null 2>&1 &

SUCCESS=$?

if [ $SUCCESS -eq 0 ]; then
    echo "Apache Config file protection: $Green[OK]$Color_Off"
else
    echo "Apache Config file protection: $Red[ERROR]$Color_Off"
fi


# Restart Apache
echo -e "$Cyan \n Restarting Apache $Color_Off"
service apache2 restart