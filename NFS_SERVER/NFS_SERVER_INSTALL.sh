#!/bin/bash

# Install required packages
apt-get update
apt-get install -y nfs-kernel-server ufw

# Setup firewall
ufw enable
ufw allow 22/tcp    # SSH
ufw allow 2049/tcp  # NFSv4

# Configure NFS to serve only via NFSv4
echo "RPCNFSDOPTS=\"-N 2 -N 3\"" | tee -a /etc/default/nfs-kernel-server
sed -i 's/^#Domain = localdomain/Domain = localdomain/' /etc/idmapd.conf
sed -i 's/^#Nobody-User = nobody/Nobody-User = nobody/' /etc/idmapd.conf
sed -i 's/^#Nobody-Group = nogroup/Nobody-Group = nogroup/' /etc/idmapd.conf

# Restart NFS services
systemctl restart nfs-kernel-server
systemctl restart nfs-idmapd

# Create shared directory
mkdir -p /srv/nfs_share/backups
chown nobody:nogroup /srv/nfs_share/backups

# Allow SSH Password authentication
sed -i 's/^#PasswordAuthentication yes/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl restart sshd
