#!/bin/bash

NFS_SERVER_IP="CHANGE-ME"
MOUNT_POINT="/mnt/nfs_share_backup"
SSH_USER="CHANGE-ME"

# Get the public IP of the distant server
PUBLIC_IP=$(curl -s ifconfig.me)

# SSH and update /etc/exports on NFS server
read -p "Enter SSH password: " -s SSHPASS
echo
SSH_CMD="if ! grep -q '${PUBLIC_IP}' /etc/exports; then echo '/srv/nfs_share/backups ${PUBLIC_IP}(rw,sync,no_subtree_check,root_squash)' | sudo tee -a /etc/exports > /dev/null && sudo exportfs -ra; fi"
echo $SSHPASS | sshpass -p $SSHPASS ssh -o StrictHostKeyChecking=no $SSH_USER@$NFS_SERVER_IP "${SSH_CMD}"

# Ensure mountpoint exists
mkdir -p ${MOUNT_POINT}

# Mount NFS share with NFSv4
mount -t nfs4 ${NFS_SERVER_IP}:/srv/nfs_share/backups ${MOUNT_POINT}

# Add to /etc/fstab only if the entry doesn't exist
FSTAB_ENTRY="${NFS_SERVER_IP}:/srv/nfs_share/backups ${MOUNT_POINT} nfs4 defaults 0 0"
if ! grep -q "${FSTAB_ENTRY}" /etc/fstab; then
    echo "${FSTAB_ENTRY}" | tee -a /etc/fstab
fi

